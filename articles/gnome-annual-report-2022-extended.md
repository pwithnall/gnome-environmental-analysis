# GNOME annual report 2022: environmental update

As a project, GNOME is directly responsible for various conferences and
server infrastructure which contribute to greenhouse gas (GHG) emissions
and therefore climate change. It's also indirectly responsible for
emissions from GNOME apps and libraries as they are used on users'
machines.

[GHG emissions](https://en.wikipedia.org/wiki/Greenhouse_gas_emissions)
are measured in equivalents of tonnes of carbon dioxide, tCO<sub>2</sub>e. Once
released into the atmosphere, GHGs contribute to global warming for
[10--200 years](https://en.wikipedia.org/wiki/Global_warming_potential)
and there is [no near-term
prospect](https://en.wikipedia.org/wiki/Direct_air_capture) of removing
them from the atmosphere at scale. In order to limit global warming to
1.5°C by the end of the century, the [annual emissions per person can be
at
most](https://en.wikipedia.org/wiki/Individual_action_on_climate_change#Suggested_individual_target_amount)
2.3tCO<sub>2</sub>e by 2030, and the earlier emissions reductions are made, the
easier it will be to hit the targets. [As of
2021](https://en.wikipedia.org/wiki/Greenhouse_gas_emissions#/media/File:20210626_Variwide_chart_of_greenhouse_gas_emissions_per_capita_by_country.svg),
average annual emissions per person in the USA were 14tCO<sub>2</sub>e, China
7tCO<sub>2</sub>e and India 2tCO<sub>2</sub>e.

In the past couple of years the project has made some first steps in
measuring these emissions, and we now have enough data to start
identifying trends and making decisions on the basis of the data in some
areas, such as conferences.

GUADEC 2022 was held in Guadalajara, Mexico, the first in-person GUADEC
since 2019 in Thessaloniki, Greece. GUADEC 2022 also had a successful
remote participation side, with multiple speakers presenting remotely,
and an average of around 1300 people viewing remotely on the core days.
Some of those remote speakers and attendees joined from a remote
attendance party in Berlin. We [collected travel
data](https://gitlab.gnome.org/pwithnall/gnome-environmental-analysis/-/tree/main/guadec-2022)
from attendees as part of our post-conference survey, and by measuring
resource usage on the conference servers.

The total transport emissions for GUADEC 2022 are similar to those for
GUADEC 2019 at around 125±20tCO<sub>2</sub>e. That's around 1tCO<sub>2</sub>e per
in-person attendee, or 40% of their target emissions. More people
attended overall (significantly through the online streaming), and
slightly fewer attended in person than in 2019. Around 10% of the
registered attendees joined from the remote attendance party in Berlin,
and their average transport emissions were a factor of 10 lower than
those travelling to Mexico. The emissions from providing the remote
participation and streaming were 2.8tCO<sub>2</sub>e, which was an insignificant
part of the conference's overall emissions, and is comparable with
emissions from GUADEC 2020 which was online-only.

Overall, the introduction of remote participation for GUADEC has
increased attendance by a factor of 10, with insignificant additional
environmental impact. The localised attendance party in Berlin reduced
the emissions for those attending it by a factor of 10, from an
unsustainable 40% of each person's 2030 target annual emissions, to
about 4%.

Other GNOME conferences have not had data analysed so far. Other big
unknowns in the GNOME project's emissions are those from project hosting
and CI infrastructure, those from the Foundation as an organisation, and
the emissions from end users using our software. Some of those things
are easier to measure and change than others, and help is always
welcome. This is something the project needs to continue making progress
on. Climate change affects us all, increasingly so, and it will
disproportionately affect those least well-positioned to deal with it.
