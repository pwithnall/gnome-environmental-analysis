GUADEC 2020 environmental analysis
===

This directory contains the results of analysing the carbon emissions of GUADEC
2020, which was held entirely online due to the COVID-19 pandemic. This seemed
like a good opportunity to estimate the environmental impact of an online-only
conference, so that the environmental impact could be included as one of several
factors for comparing the benefits and disadvantages of in-person vs virtual
conferences in future.

Scope
---

GUADEC 2020 was entirely online, with no in-person segments, and no travel by
any of the speakers, attendees or organisers in order to set up or run the
conference.

The conference used three physical servers (`gesture`, `meet` and `scale`),
which were [`m1-xlarge` machines from Packet](https://www.packet.com/cloud/servers/m1-xlarge/).
The exact model of this type of server is not known, and its power usage had to
be estimated. The carbon intensity of the power supply to Packet data centres is
known to be 92% renewable (as of 2019):
https://sustainability.equinix.com/environment/renewable-energy/

The conference had no merchandise, speaker gifts, additional hardware (such as
microphones or speakers), etc.

I am not entirely sure of the network structure within the VMs, so it’s possible
that traffic has been double-counted. This adds an error of potentially +100kg.

The analysis below _does_ include time spent watching the conference by each
user, but does not include time spent watching recordings after the conference
had ended (for example, on YouTube).

Data collection
---

Data was collected from all the relevant physical machines and VMs during GUADEC
(the core days, plus a day or two beforehand and several days after the
conference was over) using the `co2-stats.service` script (commit
737f55f069d101dab31cb5c803ad27f8d9646693 in this repository).

See https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/372 for
details of the collection, plus the raw results.

The JSON files in this directory are an unedited unzipping of those results,
with a few lines of unrelated output deleted from the start of a couple of the
files.

Analysis
---

The raw results from running `./process.py *.json` are: total emissions of
423kgCO2e (of which 234kgCO2e were from the network, 87kgCO2e were from the
servers, and 101kgCO2e were from the attendees).

Tweaking various of the inputs gives a variance of a factor of 2. So the
emissions from the conference are likely on the order of 1tCO2e or less.
