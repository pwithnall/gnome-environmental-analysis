GNOME environmental analysis
===

This repository is a collection of analysis reports, articles and data
collection scripts designed to quantify the GNOME project’s environmental
impact.

Analysis about the emissions from GUADEC conferences live in the `guadec`
directories.

`co2-stats` is a simple systemd service to collect data for estimating the
carbon emissions of a computer system. It’s used to estimate the emissions
from the remote attendance servers for GUADEC.

Various articles about GNOME’s environmental impact are collected in the
`articles` directory.
