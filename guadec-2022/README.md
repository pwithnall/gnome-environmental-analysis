GUADEC 2022 environmental analysis
===

This directory contains the results of analysing the carbon emissions of GUADEC
2022, which was a hybrid remote and in-person event. The analysis here only
covers the remote side of the conference.

Scope
---

GUADEC 2022 was a hybrid online and in-person event. Some of the participants
and speakers attended remotely, whereas some travelled to attend in-person in
Guadalajara, Mexico. There were also a few ‘remote attendance’ parties where
people travelled locally to attend the conference remotely as a group, with a
mini-GUADEC happening in Berlin.

Data is available from a post-conference survey about people’s attendance and
transport to the conference, plus from a data collection script on the server
hosting the virtual side of the conference.

The survey had a low response rate of 57 out of 305 registered conference
attendees (19%; with 23 responses from 183 remote registrations, and 34 from 122
in-person registrations). The number of in-person attendees should roughly match
the number of in-person registrations, but from the streaming viewer counts
there are demonstrably many more remote attendees than remote registrations.
3 in-person attendees who completed the survey did not provide travel data. All
of the travel questions in the survey are intentionally slightly vague, to avoid
collecting identifiable information about where people live.

The conference used one physical server (`bbb`), which was a
[`m1-xlarge` machine from Packet](https://www.packet.com/cloud/servers/m1-xlarge/).
The exact model of this type of server is not known, and its power usage had to
be estimated. The carbon intensity of the power supply to Packet data centres is
known to be 96% renewable (as of 2022):
https://sustainability.equinix.com/environment/renewable-energy-scaling-our-impact/

The conference had merchandise, speaker gifts, additional hardware (such as
microphones and speakers), etc., but none of that has been accounted for in this
anaylsis. It is likely that these emissions are small in comparison to the
transport emissions.

No data is available about emissions at the conference (such as for powering the
building, providing refreshments, merchandise, speaker gifts, additional A/V
hardware, parties, etc.). It is likely that these emissions are small in
comparison to the transport emissions.

I am not entirely sure of the network structure within the VMs, so it’s possible
that traffic has been double-counted. This adds an error of potentially +100kg.

The analysis below _does_ include time spent watching the conference by each
user, but does not include time spent watching recordings after the conference
had ended (for example, on YouTube).

Data collection
---

In-person data was collected using a post-conference survey. The privacy
restrictions on the survey results mean that only processed aggregate data can
be shared publicly (in this repository). The ODS files in this directory are the
result of that post-processing.

The continent of residence for in-person attendees was collected from
registration data, and covers all registered in-person attendees, not just those
who filled out the post-conference survey.

Remote viewing data was collected from all the relevant physical machines and
VMs during GUADEC (the core days, plus a day or two beforehand and several days
after the conference was over) using the `co2-stats.service` script (commit
737f55f069d101dab31cb5c803ad27f8d9646693 in this repository).

See https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/836 for
details of the collection, plus the raw results.

The JSON files in this directory are an unedited unzipping of those results.

Analysis
---

Initial aggregation of the survey data was done in the private survey results
spreadsheet, including manually tagging each survey response with its transport
carbon emissions using https://www.carbonfootprint.com/calculator.aspx. The
aggregate statistics copied to `post-conference-survey-summary.ods` for further
analysis, which is done in formulae and graphs in that spreadsheet.

A series of other assumptions were made about potential flight starting points
and driving distances, given the country and travel time answers in survey
responses. An emissions rate of 0.44kgCO2e/hour of travel on European trains
(https://www.sncf.com/en/commitments/sustainble-development/leading-the-charge-for-the-planet)
was used for European train travel for some of the remote attendance party
attendees.

From the survey, total transport emissions were 18923kgCO2e, of which
18560kgCO2e came from flights. The total emissions were split 18033kgCO2e (95%)
and 890kgCO2e (5%) between in-person and remote attendees, as ‘remote attendees’
also included those who travelled to the mini-GUADEC in Berlin. The mean
emissions per in-person attendee was 901kgCO2e, and it was 88kgCO2e for
attendees to the Berlin mini-GUADEC.

This is likely an underestimate of total transport emissions by a factor of
roughly 6 due to the response rate of the survey (20 valid in-person survey
responses vs 122 in-person attendees). However, due to the low absolute number
of survey responses, scaling it up linearly will introduce a potentially large
error. The distribution of continents/countries of origin for in-person
attendees (from registration data) matches the survey data for attendees from
Mexico (50% of responses and 54% of in-person attendees), but survey responses
from the rest of North America (30% vs 18%) are over-represented in the data,
and responses from Europe (5% vs 13%) are under-represented.

Estimating the flight emissions for in-person attendees by using their
continent/country of origin (from the registration data) gives in-person
transport emissions of 125tCO2e, which matches the simply scaled estimate from
the survey data quite well.

That gives overall transport emissions of around 126±20tCO2e. This is similar
to the
[transport emissions calculated for GUADEC 2019](https://gitlab.gnome.org/pwithnall/gnome-environmental-analysis/-/tree/main/guadec-2019).

For the remote data, the raw results from running `./process.py *.json` are:
total emissions of 1365kgCO2e (of which 8kgCO2e were from the network, 28kgCO2e
were from the servers, and 1328kgCO2e were from the attendees).

Tweaking various of the inputs gives a variance of a factor of 2. So the
emissions from the remote side conference are likely on the order of 2.8tCO2e
or less. This is insignificant compared to the transport emissions.

