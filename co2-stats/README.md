co2-stats.service
===

A simple systemd service to collect data which can be used to crudely estimate
the  carbon emissions (equivalent) of a system, in terms of its power
consumption and network usage.

Usage
---

Drop `co2-stats.{service,timer}` into a systemd service directory, enable and
start them, and later collect the results from
`/var/log/$(hostname).co2-stats.json`. The resulting JSON file can be processed
using `process.py` (examples of which are in other directories, as they do
additional processing specific to the event being analysed).
