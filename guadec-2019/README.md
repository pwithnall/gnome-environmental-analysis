GUADEC 2019 environmental analysis
===

This directory contains the results of analysing the post-conference survey from
GUADEC 2019, which was held in Thessaloniki, Greece.

Scope
---

GUADEC 2019 was held in Thessaloniki, Greece. Data is available from the
post-conference survey about people’s transport to the conference. There is also
limited data available from people who participated remotely or who caught up
with the conference via recordings afterwards.

The survey had a response rate of 101 out of 198 conference attendees (51%).
4 people who completed the survey did not provide travel data. Some of the
travel data which people did provide is vague (for example, saying they flew
from the US to Thessaloniki, but no further detail).

No data is available about emissions at the conference (such as for powering the
building, providing refreshments, merchandise, parties, etc.) or for emissions
from the conference server infrastructure. It is likely that these emissions are
small in comparison to the transport emissions.

Data collection
---

Data was collected using a post-conference survey. The privacy restrictions on
the survey results mean that only processed aggregate data can be shared
publicly (in this repository). The ODS files in this directory are the result of
that post-processing.

Analysis
---

Initial aggregation of the data was done in the private survey results
spreadsheet, including manually tagging each survey response with its transport
carbon emissions using https://www.carbonfootprint.com/calculator.aspx. The
aggregate statistics copied to `post-conference-survey-summary.ods` for further
analysis, which is done in formulae and graphs in that spreadsheet.

Total transport emissions were 55062kgCO2e, of which 54210kgCO2e came from
flights.

This is likely an underestimate by a factor of roughly 2 due to the response
rate of the survey. Approximately 7 local attendees responded to the survey
(assuming that anyone whose transport mode was ‘did not travel’, ‘bus’ or ‘car’
was local), out of 101 respondees (7%). 23 of 198 attendees overall were local
(12%), so the survey responses are roughly representative of the overall makeup
of attendees.

So the conference travel emissions are on the order of 110tCO2e.
