GUADEC 2023 environmental analysis
===

This directory contains the results of analysing the carbon emissions of GUADEC
2023, which was a hybrid remote and in-person event. The analysis here only
covers the remote side of the conference.

Scope
---

GUADEC 2023 was a hybrid online and in-person event. Some of the participants
and speakers attended remotely, whereas some travelled to attend in-person in
Riga, Latvia. Unlike in 2022, there were no (known) organised ‘remote
attendance’ parties where people travelled locally to attend the conference
remotely as a group.

Data is available from a post-conference survey about people’s attendance and
transport to the conference. Unfortunately, data from the server hosting the
virtual side of the conference
[was lost](https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/1038#note_2158631).

The survey had a reasonable response rate of 75 out of 275 registered conference
attendees (27%; with 32 responses from 145 remote registrations, and 43 from 130
in-person registrations). The number of in-person attendees should roughly match
the number of in-person registrations, but from the streaming viewer counts
there are demonstrably an order of magnitude more remote attendees than remote
registrations. All the in-person attendees who completed the survey provided
travel data. All of the travel questions in the survey are intentionally
slightly vague, to avoid collecting identifiable information about where people
live.

The conference used one physical server (`bbb`), which was a
[`m1-xlarge` machine from Packet](https://www.packet.com/cloud/servers/m1-xlarge/).
The exact model of this type of server is not known, and its power usage had to
be estimated. The carbon intensity of the power supply to Packet data centres is
known to be 96% renewable (as of 2022):
https://sustainability.equinix.com/environment/renewable-energy-scaling-our-impact/

Although data about the power usage from this server
[was lost](https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/1038#note_2158631),
its importance in the conference setup was lower than in previous years. This
year, the local team streamed directly from the local [OBS](https://obsproject.com/)
to YouTube and to Big Blue Button, so most of the viewers watched through
YouTube. We do not have emissions figures for YouTube. Questions were largely
asked via Matrix rather than Big Blue Button. It is likely that these emissions
are small in comparison to the transport emissions.

The conference had merchandise, speaker gifts, additional hardware (such as
microphones and speakers), etc., but none of that has been accounted for in this
anaylsis. It is likely that these emissions are small in comparison to the
transport emissions.

No data is available about emissions at the conference (such as for powering the
building, providing refreshments, merchandise, speaker gifts, additional A/V
hardware, parties, etc.). It is likely that these emissions are small in
comparison to the transport emissions.

The analysis below _does_ include time spent watching the conference by each
user, but does not include time spent watching recordings after the conference
had ended (for example, on YouTube).

Data collection
---

In-person data was collected using a post-conference survey. The privacy
restrictions on the survey results mean that only processed aggregate data can
be shared publicly (in this repository). The ODS files in this directory are the
result of that post-processing.

The continent of residence for in-person attendees was collected from
registration data, and covers all registered in-person attendees, not just those
who filled out the post-conference survey.

Remote viewing data was meant to be collected from all the relevant physical
machines and VMs during GUADEC using the `co2-stats.service` script (commit
737f55f069d101dab31cb5c803ad27f8d9646693 in this repository), but as mentioned
above, this did not work, so this data is not available.

Catering
---

A back-of-the-envelope aside to estimate how much impact the conference catering
had on emissions.

The in-person food at lunch was largely provided by the venue’s canteen. There
was a variety of menu options, although relatively few vegetarian or vegan
options. If we assume 120 of the 130 in-person conference attendees ate at the
canteen for the 3 core conference days, that’s 360 meals — or roughly 1/3 of the
average annual footprint of a diet. For an average omnivorous diet, the
footprint is 2.5tCO2e; for a vegetarian diet, 1.7tCO2e; and for a vegan diet,
1.5tCO2e (source: https://shrinkthatfootprint.com/food-carbon-footprint-diet/).

So if all 120 people ate an omnivorous menu, catering emissions from conference
lunches would be around 830kgCO2e. If they all ate a vegan menu, those emissions
would be 500kgCO2e. These numbers are insignificant compared to any of the
transport emissions, although they are more directly under the control of the
conference organisers than attendees’ travel is.

Analysis
---

Initial aggregation of the survey data was done in the private survey results
spreadsheet, including manually tagging each survey response with its transport
carbon emissions using https://www.carbonfootprint.com/calculator.aspx. The
aggregate statistics were copied to `post-conference-survey-summary.ods` for
further analysis, which is done in formulae and graphs in that spreadsheet.

A series of other assumptions were made about potential flight starting points
and driving distances, given the country and travel time answers in survey
responses. An emissions rate of 0.44kgCO2e/hour of travel on European trains
(https://www.sncf.com/en/commitments/sustainble-development/leading-the-charge-for-the-planet)
was used for European train travel.

From the survey, total transport emissions were 55836kgCO2e, of which
55320kgCO2e came from flights. The total emissions were split 52885kgCO2e (95%)
and 2951kgCO2e (5%) between in-person and remote attendees. The emissions for
the remote attendees include one flight, as one survey respondee clearly
indicated they flew (to somewhere other than Riga) to attend remotely — we have
no further detail than this. The mean emissions per in-person attendee was
1230kgCO2e, and it was 92kgCO2e for those attending remotely.

This is likely an underestimate of total transport emissions by a factor of
roughly 3 due to the response rate of the survey (43 valid in-person survey
responses vs 130 in-person attendees). Due to the unknown distribution of
in-person survey responses by country of origin, scaling it up will potentially
introduce a large error. We do not have correlated data about people’s origin
country vs their travel mode or duration, so we cannot use a better approach.
Similarly, the outlier of one flight for one of the remote attendees may skew
a simply-scaled remote attendee total transport emissions figure — although the
mean figure of 92kgCO2e for remote attendees is similar to the figure of
88kgCO2e per remote attendee for GUADEC 2022.

Accordingly, the estimated total transport emissions is 173tCO2e (comprising
160tCO2e for in-person attendees and 13tCO2e for remote attendees).

This is about 37% higher than the
[transport emissions calculated for GUADEC 2022](https://gitlab.gnome.org/pwithnall/gnome-environmental-analysis/-/tree/main/guadec-2022).
The increase comes from higher mean per-person transport emissions for in-person
attendees (1230kgCO2e in 2023 vs 901kgCO2e in 2022), which appears to be the
result of more people flying rather than using low-carbon transport.

We don’t have server data for this year, but from the analysis done in previous
years the server emissions will be on the order of 5tCO2e, which is
insignificant compared to any of the transport emissions.

